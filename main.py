from library.api_calls import ApiCalls
import csv
import math

def main():
    # Authentication
    instance = 'https://<org>.atlassian.net'
    username = '<your e-mail to access Jira>'
    password = '<your API Token>'
    
    # Set Authentication
    api_call = ApiCalls()
    api_call.setAuth('basic', username=username, password=password)

    # jql quqery
    project_name = 'Scrum 1'
    sprint_name = 'S1 Sprint 1'
    status_name = 'To Do'

    jql = "project = '" + project_name + "' and (sprint = '" + sprint_name + "' and status = '" + status_name + "')"
    fields = ['key',  'id', 'aggregateprogress', 'assignee']

    # Initial values
    max_result = 100
    start_at = 0
    diff = 100
    counter = 0

    # Creating the csv file as iterating through issues
    with open('reportTest.csv', mode='w') as story_points_report:
        report_writer = csv.writer(story_points_report, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        # Number of columns must be equal to the number of fields returned
        rows = [
            ['Project name: ' + project_name, '', '', '', ''],
            ['Sprint name: ' + sprint_name, '', '', '', ''],
            ['Mapped status: ' + status_name, '', '', '', ''],
            ['', '', '', '', '']
        ]
        report_writer.writerows(rows)

        report_writer.writerow(['Key', 'ID', 'Assignee', 'Worked Hours', 'Progress'])

        while diff > 0:
            json_query = {
                'jql': jql,
                'startAt': start_at,
                'maxResults': max_result,
                'fields': fields
            }

            # API call
            response = api_call.request(
            'POST', instance + '/rest/api/3/search', json_data=json_query)
            response_json = response.json()

            for issue in response_json['issues']:
                size_of_progress_info = len(issue['fields']['aggregateprogress'])

                if size_of_progress_info == 2:
                    progress = '0%'
                    worked_hours = '0'
                else:
                    progress = str(issue['fields']['aggregateprogress']['percent']) + '%'
                    total_seconds_logged = issue['fields']['aggregateprogress']['progress']
                    worked_hours = str(math.ceil(total_seconds_logged / 3600))

                assignee = issue['fields']['assignee']
                if assignee is None:
                    assignee = 'None'
                else:
                    assignee = issue['fields']['assignee']['displayName']
                report_writer.writerow([issue['key'], issue['id'], assignee, worked_hours, progress])

            counter += 1

            start_at = counter * max_result
            diff = response_json['total'] - start_at


if __name__ == '__main__':
    main()
